variable "additional_tags" {
  default     = {}
  description = "map of names of the repository inferred by directory name"
  type        = map(string)
}
variable "name" {
  description = "map of names of the repository inferred by directory name"
  type        = map(any)
}
variable "name_with_additional_tags" {
  default     = []
  description = "list of names with additional tag"
  type        = list(any)
}
variable "name_without_additional_tags" {
  default     = []
  description = "list of names without additional tag"
  type        = list(any)
}
variable "role_arn" {
  description = "assumed to create infrastructure in enviroment where .hcl is ran"
  type        = string
}
variable "tag" {
  default = {
  }
  description = "tag to be added"
  type        = map(any)
}
variable "use_always_additional_tags" {
  default     = false
  description = "if set to true name_with_additional_tags is like contains '*'"
  type        = bool

}


