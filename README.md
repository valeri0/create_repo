## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 0.15.0, < 2.0.0 |
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | ~> 3.32.0 |

## Providers

No providers.

## Modules

| Name | Source | Version |
|------|--------|---------|
| <a name="module_codecommit_service"></a> [codecommit\_service](#module\_codecommit\_service) | git::https://bitbucket.org/valeri0/codecommit.git | 0.5.0 |

## Resources

No resources.

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_additional_tags"></a> [additional\_tags](#input\_additional\_tags) | map of names of the repository inferred by directory name | `map(string)` | `{}` | no |
| <a name="input_name"></a> [name](#input\_name) | map of names of the repository inferred by directory name | `map(any)` | n/a | yes |
| <a name="input_name_with_additional_tags"></a> [name\_with\_additional\_tags](#input\_name\_with\_additional\_tags) | list of names with additional tag | `list(any)` | `[]` | no |
| <a name="input_name_without_additional_tags"></a> [name\_without\_additional\_tags](#input\_name\_without\_additional\_tags) | list of names without additional tag | `list(any)` | `[]` | no |
| <a name="input_role_arn"></a> [role\_arn](#input\_role\_arn) | assumed to create infrastructure in enviroment where .hcl is ran | `string` | n/a | yes |
| <a name="input_tag"></a> [tag](#input\_tag) | tag to be added | `map(any)` | `{}` | no |
| <a name="input_use_always_additional_tags"></a> [use\_always\_additional\_tags](#input\_use\_always\_additional\_tags) | if set to true name\_with\_additional\_tags is like contains '*' | `bool` | `false` | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_role_arn"></a> [role\_arn](#output\_role\_arn) | default role |
