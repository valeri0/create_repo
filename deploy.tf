module "codecommit_service" {
  additional_tags              = var.additional_tags
  name                         = var.name
  name_with_additional_tags    = var.name_with_additional_tags
  name_without_additional_tags = var.name_without_additional_tags
  source                       = "git::https://bitbucket.org/valeri0/codecommit.git?ref=0.6.0"
  #source                    = "/Users/vcrini/Repositories/terraform-modules/codecommit"
  use_always_additional_tags = var.use_always_additional_tags
  tags                       = var.tag
}
